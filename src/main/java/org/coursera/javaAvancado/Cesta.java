package org.coursera.javaAvancado;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class Cesta <TipoGenerico> {
	
	public List<TipoGenerico> list = new ArrayList<>();

	public void adiciona(TipoGenerico tipoGenerico) {
		list.add(tipoGenerico);
	}
	
	public TipoGenerico retira() {
		Random random = new Random(System.currentTimeMillis());
		return list.remove(random.nextInt(list.size()));
	}
	
	public boolean temItens() {
		return !list.isEmpty();
	}
	
	public void adicionaTodos(Collection<? extends TipoGenerico> tipoGenerico) {
		
		for(TipoGenerico tipo: tipoGenerico) {
			adiciona(tipo);
		}
	}
}
